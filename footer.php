<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */
?>




</section><!-- #main -->

<footer id="contact" class="footer-section" role="contentinfo">
    <div class="container">
        <div class="row">
            <div class="col-sm-7">
                <div data-aos="zoom-in-up" data-aos-anchor-placement="top-bottom">
                <?php if( have_rows('footer_info','option') ): ?>
                <ul class="footer-info">
                <?php while ( have_rows('footer_info','option') ) : the_row(); ?>
                <li><a <?php the_sub_field('href'); ?> <?php if(get_sub_field('new_tab')): ?>target="_blank" <?php endif; ?>><span class="flabel"><?php the_sub_field('label'); ?></span> <?php the_sub_field('title'); ?></a></li>
                <?php endwhile; ?>
                </ul>
                <?php endif; ?>
                </div>

            </div>
            <div class="col-sm-5"><div data-aos="zoom-in-up" data-aos-anchor-placement="top-bottom"><div class="footer_logo">
                <?php if(get_field('m_cafe_footer_logo')): ?>
                <div class="m_cafe_footer_logo"><img src="<?php the_field('m_cafe_footer_logo'); ?>" alt=""></div>
                <?php else: ?>
                <a  href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php the_field('footer_logo', 'option'); ?>" alt=""></a>
                <?php endif; ?>
                </div></div></div>
            <?php if(get_field('show_footer_menu', 'option')): ?>
            <div class="col-md-12">
                <div data-aos="zoom-in-up" data-aos-anchor-placement="top-bottom">
                <?php wp_nav_menu( array(  'menu' => 'Footer Menu') ); ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</footer><!-- footer -->
<?php if(get_field('map','option')): ?>
<div class="map_popup transitionAll">
    <a class="mapcloseicon" href="javascript:void(0);"><img src="<?php bloginfo('template_directory'); ?>/images/close_icon.svg" alt=""></a>
    <?php the_field('map','option'); ?>
</div>
<?php endif; ?>

<?php wp_footer(); ?>
<script>
function goBack() {
    window.history.back();
}
    AOS.init({
      duration: 300
    });
</script>
</div>
</body>
</html>
