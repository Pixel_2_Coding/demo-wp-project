<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */

get_header(); ?>

<section class="bloc_section bs_two_column align-self-center" style="<?php if(get_sub_field('background_color')):?>background-color:<?php the_sub_field('background_color'); ?>;<?php endif; ?><?php if(get_sub_field('text_color')):?>color:<?php the_sub_field('text_color'); ?>;<?php endif; ?>">
    <div class="container"> 
        <?php if(get_field('heading_404','option') || get_field('sub_heading_404','option')): ?>
        <div class="text-center sec_head">
            <?php if(get_field('heading_404','option')): ?>
            <h2 class="heading"><?php the_field('heading_404','option'); ?></h2>
            <?php endif; ?>
            <?php if(get_field('sub_heading_404','option')): ?>
            <h3 class="sub_heading"><?php the_field('sub_heading_404','option'); ?></h3>
            <?php endif; ?>
        </div>
        <?php endif; ?>
        <div class="description_404 text-center"><?php the_field('description_404','option'); ?></div>
    </div>
</section>


<?php get_footer(); ?>
