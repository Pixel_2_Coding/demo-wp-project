<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */

get_header(); ?>



<div class="news_bar">
    <div class="container clearfix">
        <div class="news_back"><a onclick="goBack()" href="Javascript:void(0);" ><span>&lt;</span> BACK</a></div>
        <div class="news_heading"><?php echo apply_filters( 'the_title', get_the_title( get_option( 'page_for_posts' ) ) ); ?></div>
        <div class="news_search"><a href="Javascript:void(0);"><img src="<?php bloginfo('template_directory'); ?>/images/icon_search.svg" alt=""></a></div> 
    </div>
    <div class="search_bar">
    <div class="container">
        <form action="/" method="get">
            <div class="clearfix search_bg"><input type="text" name="s" id="search" placeholder="Search..." value="<?php the_search_query(); ?>" /><button type="submit"><i class="fas fa-search"></i></button>
            </div>
        </form>
    </div>
</div>

</div>
<?php 

$args = array(
    'post_type' => 'post',
    'posts_per_page' => -1
);


$query = new WP_Query( $args );


if ( $query->have_posts() ): ?> 
<div class="container">
    <div class="row">
<?php $p=0; while ( $query->have_posts() ) :  $query->the_post(); $p++; ?>
    
<?php if($p <= 2): ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class('large_post col-md-12'); ?>>
    <div class="row">
        <div class="<?php if ( has_post_thumbnail() ): ?>col-md-7 order-2 order-md-1<?php else: ?>col-md-12<?php endif; ?>">
            <div class="post_content">
            <h2><?php the_title(); ?></h2>
            <div class="post_excerpt"><?php the_excerpt(); ?></div>
            <div class="post_url"><a href="<?php the_permalink(); ?>">...READ MORE</a></div>
            </div>
        </div>
        <?php if ( has_post_thumbnail() ): ?>
        <div class="col-md-5 post-image order-1 order-md-2">
            <?php the_post_thumbnail('post-slider'); ?>
        </div>
        <?php endif; ?> 
    </div>
        <div class="divider"></div>
        
</article><!-- #post-## -->
    <?php else: ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class('small_post col-md-4 jQueryEqualHeight'); ?>>
    
        <div class="post-box cardbox" data-aos="zoom-in-up" data-aos-delay="100">
            <?php if ( has_post_thumbnail() ): ?>
            <?php the_post_thumbnail('post-small'); ?>
            <?php else: ?>
            <img src="<?php bloginfo('template_directory'); ?>/images/no_image.jpg" alt="">
            <?php endif; ?> 
            <div class="post_content">
            <h2 cl><?php the_title(); ?></h2>
            <div class="post_excerpt"><?php echo get_excerpt(195); ?></div>
            <div class="post_url"><a href="<?php the_permalink(); ?>">...READ MORE</a></div>
            </div>
        </div>
        
    
</article><!-- #post-## -->
    <?php endif; ?>
    
<?php endwhile; ?>
</div>
</div>
<?php endif; wp_reset_postdata(); ?>

<?php /*<div class="container text-center"><?php wp_pagenavi(); ?></div>*/ ?>

<?php get_footer(); ?>