<?php

//Disable Frontend emoji support. WHy is this even here????
//Copied from https://wordpress.org/plugins/disable-emojis/
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );	
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );	
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	//add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}
add_action( 'init', 'disable_emojis' );

##### Disable XMLRPC
add_filter( 'xmlrpc_enabled', '__return_false' );
add_theme_support( 'post-thumbnails' ); 
add_theme_support( 'menus' );
add_theme_support( 'title-tag' );
register_nav_menus( array(
        'primary' => __( 'Primary Menu', 'studiostech' ),
) );




function studiostech_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'studiostech' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'studiostech' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
    register_sidebar( array(
		'name'          => __( 'Footer Section', 'studiostech' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'studiostech' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
     register_sidebar( array(
		'name'          => __( 'Footer Social Icon', 'studiostech' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'studiostech' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );


 

  
}
add_action( 'widgets_init', 'studiostech_widgets_init' );

function studiostech_scripts() {
	wp_enqueue_script( 'jquery', get_theme_file_uri( '/js/jquery.js' ), array(), '3.7.3' );
	wp_enqueue_script( 'smoothscroll', get_theme_file_uri( '/js/smoothscroll.js' ), array(), '3.7.3' ); 
	wp_enqueue_script( 'jquery-equal-height', get_theme_file_uri( '/js/jquery-equal-height.js' ), array(), '3.7.5' ); 
    
    // Google fonts
    //wp_enqueue_style( 'montserrat', 'https://fonts.googleapis.com/css?family=Montserrat', array(), '3.7.3' );
	//wp_enqueue_style( 'arvo', 'https://fonts.googleapis.com/css?family=Arvo:400,400i,700', array(), '3.7.3' );
	//wp_enqueue_style( 'signika', 'https://fonts.googleapis.com/css?family=Signika+Negative:300,400,600,700', array(), '3.7.3' );
    
    wp_enqueue_style( 'fontawesome', get_theme_file_uri( 'fontawesome/css/all.min.css' ), array(), '1.0' );
	
    
    
    // Bootstrap
    wp_enqueue_style('bootstrap', get_theme_file_uri( '/css/bootstrap.min.css'), array(), '3.7.3' );
    //wp_enqueue_script('slim', get_theme_file_uri( '/js/jquery-3.3.1.slim.min.js'), array(), '1.0' );
    wp_enqueue_script('popper', get_theme_file_uri( '/js/popper.min.js'), array(), '1.0' );
    wp_enqueue_script('bootstrap', get_theme_file_uri( '/js/bootstrap.min.js'), array(), '1.0' );
    
    
    // slider.
    wp_enqueue_script( 'AOS', get_theme_file_uri( '/js/aos.js' ), array(), '3.7.3' );
    wp_enqueue_style( 'AOS', get_theme_file_uri( '/css/aos.css' ), array(), '1.0' );
    
    // AOS Animation
    wp_enqueue_script( 'slider', get_theme_file_uri( '/slider/slick.min.js' ), array(), '3.7.3' );
    wp_enqueue_style( 'slider', get_theme_file_uri( '/slider/slick.css' ), array(), '1.0' );
    
	// Theme stylesheet.
	wp_enqueue_style( 'site-style', get_stylesheet_uri(), array(), date("m-d-Y H:i:s.u") );
    //wp_enqueue_style( 'style1', get_theme_file_uri( '/style1.css' ), array(), '1.0' );
    
    // Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'site-ie8', get_theme_file_uri( '/css/ie8.css' ), array( 'site-style' ), '1.0' );
	wp_style_add_data( 'site-ie8', 'conditional', 'lt IE 9' );

	// Load the html5 shiv.
	wp_enqueue_script( 'html5', get_theme_file_uri( '/js/html5.js' ), array(), '3.7.3' );
	wp_script_add_data( 'html5', 'conditional', 'lt IE 9' );
    
    // Custom
    wp_enqueue_script( 'script', get_theme_file_uri( '/js/script.js' ), array(), date("m-d-Y H:i:s.u") );
	
}
add_action( 'wp_enqueue_scripts', 'studiostech_scripts' );


// Register Custom Navigation Walker
//require_once get_template_directory() . '/wp-bootstrap-navwalker.php';


add_image_size( 'box-thumb', 450, 637, true );

//Blog Sizes
add_image_size( 'post-slider', 547, 621, true );
add_image_size( 'post-big', 546, 620, true );
add_image_size( 'post-small', 431, 489, true );

// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');



if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}






function menu_post() {

	$labels = array(
		'name'                  => _x( 'Menu', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Menu', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Menu', 'text_domain' ),
		'name_admin_bar'        => __( 'Menu', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New Menu', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Menu', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
		'taxonomies'            => array( 'menu_category' ),
		'hierarchical'          => false,
		'public'                => false,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'menu', $args );

}
add_action( 'init', 'menu_post', 0 );


// Register Custom Taxonomy
function menu_category() {

	$labels = array(
		'name'                       => _x( 'Menu Categories', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Menu Category', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Menu Category', 'text_domain' ),
		'all_items'                  => __( 'All Items', 'text_domain' ),
		'parent_item'                => __( 'Parent Item', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
		'new_item_name'              => __( 'New Menu Category', 'text_domain' ),
		'add_new_item'               => __( 'New Menu Category', 'text_domain' ),
		'edit_item'                  => __( 'Edit Menu Category', 'text_domain' ),
		'update_item'                => __( 'Update Menu Category', 'text_domain' ),
		'view_item'                  => __( 'View Menu Category', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Items', 'text_domain' ),
		'search_items'               => __( 'Search Items', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No items', 'text_domain' ),
		'items_list'                 => __( 'Items list', 'text_domain' ),
		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => false,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'menu_category', array( 'menu' ), $args );

}
add_action( 'init', 'menu_category', 0 );





add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy');
function tsm_filter_post_type_by_taxonomy() {
	global $typenow;
	$post_type = 'menu'; // change to your post type
	$taxonomy  = 'menu_category'; // change to your taxonomy
	if ($typenow == $post_type) {
		$selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
		$info_taxonomy = get_taxonomy($taxonomy);
		wp_dropdown_categories(array(
			'show_option_all' => __("Show All {$info_taxonomy->label}"),
			'taxonomy'        => $taxonomy,
			'name'            => $taxonomy,
			'orderby'         => 'name',
			'selected'        => $selected,
			'show_count'      => true,
			'hide_empty'      => true,
		));
	};
}


add_filter('parse_query', 'tsm_convert_id_to_term_in_query');
function tsm_convert_id_to_term_in_query($query) {
	global $pagenow;
	$post_type = 'menu'; // change to your post type
	$taxonomy  = 'menu_category'; // change to your taxonomy
	$q_vars    = &$query->query_vars;
	if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
		$term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
		$q_vars[$taxonomy] = $term->slug;
	}
}



add_filter( 'img_caption_shortcode', 'my_img_caption_shortcode', 10, 3 );

function my_img_caption_shortcode( $empty, $attr, $content ){
	$attr = shortcode_atts( array(
		'id'      => '',
		'align'   => 'alignnone',
		'width'   => '',
		'caption' => ''
	), $attr );

	if ( 1 > (int) $attr['width'] || empty( $attr['caption'] ) ) {
		return '';
	}

	if ( $attr['id'] ) {
		$attr['id'] = 'id="' . esc_attr( $attr['id'] ) . '" ';
	}

	return '<div ' . $attr['id']
	. 'class="wp-caption ' . esc_attr( $attr['align'] ) . '" '
	. 'style="max-width: ' . ( 10 + (int) $attr['width'] ) . 'px;">'
	. do_shortcode( $content )
	. '<p class="wp-caption-text">' . $attr['caption'] . '</p>'
	. '</div>';

}




// Limit except length to 125 characters.
// tn limited excerpt length by number of characters
function get_excerpt( $count ) {
$permalink = get_permalink($post->ID);
$excerpt = get_the_content();
$excerpt = strip_tags($excerpt);
$excerpt = substr($excerpt, 0, $count);
$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
//$excerpt = '<p>'.$excerpt.'... <a href="'.$permalink.'">Read More</a></p>';
$excerpt = '<p>'.$excerpt.'.</p>';
return $excerpt;
}



add_filter("gform_confirmation_anchor", create_function("","return false;"));