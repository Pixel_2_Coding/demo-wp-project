<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */
?><!DOCTYPE html>
<!--[if lt IE 7 ]><html class="no-js ie ie6 lte7 lte8 lte9 <?php if(get_field('transparent_header')): ?> transparent_header<?php endif; ?>" dir="<?php bloginfo('text_direction'); ?>" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9 <?php if(get_field('transparent_header')): ?> transparent_header<?php endif; ?>" dir="<?php bloginfo('text_direction'); ?>" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9 <?php if(get_field('transparent_header')): ?> transparent_header<?php endif; ?>" dir="<?php bloginfo('text_direction'); ?>" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9 <?php if(get_field('transparent_header')): ?> transparent_header<?php endif; ?>" dir="<?php bloginfo('text_direction'); ?>" <?php language_attributes(); ?>><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js <?php if(get_field('transparent_header')): ?> transparent_header<?php endif; ?>" dir="<?php bloginfo('text_direction'); ?>" <?php language_attributes(); ?>><!--<![endif]-->
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <style>
        @media only screen and (max-width: 1200px) {
			.box_title h3 {
				margin-bottom:0 !important;
			}
		}
        </style>
<?php
		/* We add some JavaScript to pages with the comment form
		 * to support sites with threaded comments (when in use).
		 */
		if ( is_singular() && get_option( 'thread_comments' ) )
			wp_enqueue_script( 'comment-reply' );

		/* Always have wp_head() just before the closing </head>
		 * tag of your theme, or you will break many plugins, which
		 * generally use this hook to add elements to <head> such
		 * as styles, scripts, and meta tags.
		 */
		wp_head();
?>

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125633044-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag()
        gtag('js', new Date());
        
        gtag('config', 'UA-125633044-1');
        </script>
        
        <meta name="google-site-verification" content="PeBYHeyOcv2rciMLoDu_vuF3fTuo3s9zmjpXz4-aQN8" />

	</head>
    <body <?php body_class('notFixed'); ?>>
        <div class="page_sec">
            <div class="sidebarMenu">
                <div class="sidebarmenuicon"><button class="icon_hamburger_close"><img src="<?php bloginfo('template_directory'); ?>/images/icon_hamburger_close.svg" alt=""></button></div>
                <div class="sidebar_scroller">
                            <?php
                            wp_nav_menu( array(
                                'menu'              => 'Main Menu',
                                //'theme_location'    => 'primary',
                                'container'         => 'div',
                                'menu_class'        => 'nav navbar-nav',
                                'container'         => 'div',
                                'after' => '<span class="mobileMenuArrow"></span>'
                            ) );
                            ?>

                    <?php //wp_nav_menu( array('menu' => 'Main Menu', 'container_class' => 'mainMenuScroller', 'link_before' => '<span>', 'link_after' => '</span>')); ?>

                <div class="sidebar_footer">
                    <?php if(get_field('sidebar_button_label','option')): ?>
                    <div><a class="btn btn-black-border" href="<?php the_field('sidebar_button_url','option'); ?>"><?php the_field('sidebar_button_label','option'); ?></a></div>
                    <?php endif; ?>

										<?php if( have_rows('social_media','option') ): ?>
										<div class="sidebar_social_bar clearfix"><ul class="social_media">
												<?php while ( have_rows('social_media','option') ) : the_row(); ?>
												<li><a href="<?php the_sub_field('url'); ?>" target="_blank"><i class="<?php the_sub_field('icon_class'); ?>"></i></a></li>
												<?php endwhile; ?>
										</ul>
									</div>
										<?php endif; ?>

            <?php if( have_rows('footer_info','option') ): ?>
                <ul class="sidebar-info">
                <?php while ( have_rows('footer_info','option') ) : the_row(); ?>
                <li><a <?php the_sub_field('href'); ?> <?php if(get_sub_field('new_tab')): ?>target="_blank" <?php endif; ?>><span class="flabel"><?php the_sub_field('label'); ?></span> <?php the_sub_field('title'); ?></a></li>
                <?php endwhile; ?>
                </ul>
                <?php endif; ?>
                </div>
                </div>
            </div>
            <header role="header" class="site_header transitionAll">
                <div class="header-container clearfix" data-aos="fade-down" data-aos-easing="linear" data-aos-duration="600">
                    <div class="headerLeft clearfix" >
                        <?php if( have_rows('social_media','option') ): ?>
                        <ul class="social_media">
                            <?php while ( have_rows('social_media','option') ) : the_row(); ?>
                            <li><a href="<?php the_sub_field('url'); ?>" target="_blank"><i class="<?php the_sub_field('icon_class'); ?>"></i></a></li>
                            <?php endwhile; ?>
                        </ul>
                        <?php endif; ?>
                        <div class="topMenu"><?php wp_nav_menu( array(  'menu' => 'Top Menu') ); ?></div>
												<?php if(get_field('logo_on_scroll','option')): ?>
												<div class="scroll_logo"><a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php the_field('logo_on_scroll','option'); ?>" alt="<?php bloginfo( 'name' ); ?>"></a></div>
											<?php endif; ?>
                    </div>
                    <div class="headerRight">
                        <button class="icon_hamburger"><img class="icon_hamburger_open" src="<?php bloginfo('template_directory'); ?>/images/icon_hamburger.svg" alt=""><img class="icon_hamburger_close" src="<?php bloginfo('template_directory'); ?>/images/icon_hamburger_close.svg" alt=""></button>
                        <?php /* if(is_front_page()): */?><div class="mainMenu"><div class="mainMenuContainer"><?php wp_nav_menu( array('menu' => 'Main Menu', 'container_class' => 'mainMenuScroller', 'link_before' => '<span>', 'link_after' => '</span>')); ?></div></div><?php /* endif; */?>
                    </div>
                </div>
            </header>
            <?php if(is_front_page()): ?>
            <div class="heder-container clearfix">
                    <?php if(get_field('logo','option')): ?>
                    <h1 class="site-logo"><a  href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php the_field('logo','option'); ?>" alt="<?php bloginfo( 'name' ); ?>" data-aos="zoom-in" data-aos-delay="600"></a></h1>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        <section id="content" role="main">