<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the wordpress construct of pages
 * and that other 'pages' on your wordpress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */

get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<?php if( have_rows('blocks') ):?>
<?php $i=0; while ( have_rows('blocks') ) : the_row(); $i++;
	get_template_part( 'page-templates/flexible/'.get_row_layout());
?>
<?php endwhile; ?>
<?php endif; ?>

<?php if ( $post->post_content!=="" ): ?> 
<section class="bloc_section page_content_area">
<article id="post-<?php the_ID(); ?>" <?php post_class('page-sec'); ?>> 
    <div class="container">
    <div class="container">
        <?php if ( has_post_thumbnail() ): ?>
        <div class="post_image"><?php the_post_thumbnail('full'); ?></div>
            <?php endif; ?> 
        
        <div class="entry-content">
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                    <?php the_content(); ?>
                </div>
        <div class="">
        <ul class="share-icons">
            <li><span>SHARE THIS</span></li>
            <li><a target="_blank" href="https://www.facebook.com/sharer?u=<?php the_permalink();?>&t=<?php the_title(); ?>"><i class="fab fa-facebook"></i></a></li>
            <li><a target="_blank" href="http://twitter.com/intent/tweet?text=Currently reading <?php the_title(); ?>&amp;url=<?php the_permalink(); ?>" title="Click to share this post on Twitter"><i class="fab fa-twitter"></i></a></li>
        </ul>
        </div>
    </div>
    
    
</div>
</article><!-- #post-## -->
</section>
<?php endif; ?>
<?php endwhile; ?>










<?php get_footer(); ?>


