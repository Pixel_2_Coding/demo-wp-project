<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */

get_header(); ?>
<div class="news_bar">
    <div class="container clearfix">
        <div class="news_back"><a onclick="goBack()" href="Javascript:void(0);" ><span>&lt;</span> BACK</a></div>
        <div class="news_heading"><?php echo apply_filters( 'the_title', get_the_title( get_option( 'page_for_posts' ) ) ); ?></div>
        <div class="news_search"><a href="Javascript:void(0);"><img src="<?php bloginfo('template_directory'); ?>/images/icon_search.svg" alt=""></a></div> 
    </div>
    <div class="search_bar">
    <div class="container">
        <form action="/" method="get">
            <div class="clearfix search_bg"><input type="text" name="s" id="search" placeholder="Search..." value="<?php the_search_query(); ?>" /><button type="submit"><i class="fas fa-search"></i></button>
            </div>
        </form>
    </div>
</div>

</div>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>> 
    <div class="container">
    <div class="container-news">
        <?php if ( has_post_thumbnail() ): ?>
        <div class="post_image"><?php the_post_thumbnail('full'); ?></div>
            <?php endif; ?> 
        
        <div class="entry-content">
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                    <?php the_content(); ?>
                </div>
        <div class="">
        <ul class="share-icons">
            <li><span>SHARE THIS</span></li>
            <li><a target="_blank" href="https://www.facebook.com/sharer?u=<?php the_permalink();?>&t=<?php the_title(); ?>"><i class="fab fa-facebook"></i></a></li>
            <li><a target="_blank" href="http://twitter.com/intent/tweet?text=Currently reading <?php the_title(); ?>&amp;url=<?php the_permalink(); ?>" title="Click to share this post on Twitter"><i class="fab fa-twitter"></i></a></li>
        </ul>
        </div>
    </div>
    
    <div class="clearfix blog-pagination">
<div class="prev-posts"><?php previous_post_link('%link', 'PREVIOUS...') ?></div>
<div class="next-posts"><?php next_post_link('%link', '...NEXT') ?></div>
</div>
        
        
        
        
        <?php
/*
$tags = wp_get_post_tags($post->ID);
if ($tags) {
echo '<div class="related_post"><div class="row">';
$first_tag = $tags[0]->term_id;
$args=array(
'tag__in' => array($first_tag),
'post__not_in' => array($post->ID),
'posts_per_page'=>3,
'caller_get_posts'=>1
);
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {
while ($my_query->have_posts()) : $my_query->the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class('small_post col-md-4 jQueryEqualHeight'); ?>>
    
        <div class="post-box cardbox">
            <?php if ( has_post_thumbnail() ): ?>
            <?php the_post_thumbnail('post-small'); ?>
            <?php endif; ?> 
            <div class="post_content">
            <h2 cl><?php the_title(); ?></h2>
            <div class="post_excerpt"><?php echo get_excerpt(195); ?></div>
            <div class="post_url"><a href="<?php the_permalink(); ?>">...READ MORE</a></div>
            </div>
        </div>
        
    
</article><!-- #post-## -->
 
<?php
endwhile;
}
wp_reset_query();
    echo '</div></div>';
}
*/?>
        
        
</div>
    
</article><!-- #post-## -->
<?php endwhile; ?>

<?php get_footer(); ?>