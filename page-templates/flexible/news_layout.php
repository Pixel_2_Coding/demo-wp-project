<section id="<?php the_sub_field('section_id'); ?>" class="bloc_section bs_news content_sec_<?php echo $GLOBALS['i']; ?> <?php the_sub_field('section_class'); ?>" style="<?php if(get_sub_field('background_color')):?>background-color:<?php the_sub_field('background_color'); ?>;<?php endif; ?><?php if(get_sub_field('text_color')):?>color:<?php the_sub_field('text_color'); ?>;<?php endif; ?>">
    <div class="container <?php the_sub_field('container_size');?>">
        <?php if(get_sub_field('heading') || get_sub_field('sub_heading')): ?>
        <div class="text-center sec_head" data-aos="zoom-in-up">
            <?php if(get_sub_field('heading')): ?>
            <h2 class="heading"><?php the_sub_field('heading'); ?></h2>
            <?php endif; ?>
            <?php if(get_sub_field('sub_heading')): ?>
            <h3 class="sub_heading"><?php the_sub_field('sub_heading'); ?></h3>
            <?php endif; ?>
        </div>
        <?php endif; ?>
        <?php $post_args = array('post-type' => 'post', 'post_status' => 'publish', 'posts_per_page'=> 3 ); ?>
        <?php $wp_query = new WP_Query($post_args); ?> 
        <?php if ($wp_query->have_posts()) : ?> 
        <div class="site-slider">
        <?php $o = 0; while ($wp_query->have_posts()) : $wp_query->the_post(); $o++; ?>
        <div class="news-item">
        <div class="news-item-inner">
        <div class="row">
        <div class="col-sm-7 news-details">
            <h2 data-aos="zoom-in-up"><?php the_title(); ?></h2>
            <div class="news-excerpt" data-aos="zoom-in-up"><?php the_excerpt(); ?></div>
            <div class="news-more" data-aos="zoom-in-up"><a href="<?php the_permalink(); ?>">...READ MORE</a></div>
        </div>
        <div class="col-sm-5 news-image">
            <?php if ( has_post_thumbnail() ): ?>
            <div data-aos="zoom-in-up"><?php the_post_thumbnail('post-slider'); ?></div>
            <?php endif; ?>
        </div>
        </div>
        </div>
        </div>
        <?php endwhile; ?> 
        <?php wp_reset_query(); ?>
        </div>
        <?php endif; ?>
        <?php if( have_rows('button_group') ): ?>
        <div class="button_group text-center" data-aos="zoom-in-up">
            <?php while ( have_rows('button_group') ) : the_row(); ?>
            <a class="btn btn-white" href="<?php the_sub_field('url'); ?>" <?php if(get_sub_field('new_window')): ?> target="_blank"<?php endif; ?>><?php the_sub_field('title'); ?></a>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
</section>