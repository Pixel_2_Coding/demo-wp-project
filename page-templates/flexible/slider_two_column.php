<section id="<?php the_sub_field('section_id'); ?>" class="bloc_section bs_news bs_slider_two_clm content_sec_<?php echo $GLOBALS['i']; ?> <?php the_sub_field('section_class'); ?>" style="<?php if(get_sub_field('background_color')):?>background-color:<?php the_sub_field('background_color'); ?>;<?php endif; ?><?php if(get_sub_field('text_color')):?>color:<?php the_sub_field('text_color'); ?>;<?php endif; ?>">
    <div class="container <?php the_sub_field('container_size');?>">
        <?php if(get_sub_field('heading') || get_sub_field('sub_heading')): ?>
        <div class="text-center sec_head">
            <?php if(get_sub_field('heading')): ?>
            <h2 class="heading"><?php the_sub_field('heading'); ?></h2>
            <?php endif; ?>
            <?php if(get_sub_field('sub_heading')): ?>
            <h3 class="sub_heading"><?php the_sub_field('sub_heading'); ?></h3>
            <?php endif; ?>
        </div>
        <?php endif; ?>
        <?php if( have_rows('slider') ): ?>
    
        

        
        <div class="site-slider">
            <?php $o = 0; while ( have_rows('slider') ) : the_row(); $o++; ?>
        <div class="news-item">
        <div class="news-item-inner">
        <div class="row">
        <div class="col-sm-7 news-details align-self-center">
        <div class="sl-col-2-content">
            <h2><?php the_sub_field('heading'); ?></h2>
            <div class="news-excerpt"><?php the_sub_field('description'); ?></div>
            <div class="more-btn"><a class="btn" href="<?php the_sub_field('button_url'); ?>"><?php the_sub_field('button_label'); ?></a></div>
        </div>
        </div>
        <div class="col-sm-5 news-image align-self-center">
            <?php
            $image = get_sub_field('image');

            $url = $image['url'];
            $title = $image['title'];
            $alt = $image['alt'];
            $caption = $image['caption'];
            
            // thumbnail
            $size = 'post-slider';
            $thumb = $image['sizes'][ $size ];
            $width = $image['sizes'][ $size . '-width' ];
            $height = $image['sizes'][ $size . '-height' ];
            ?>
            
            <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" />      
            </div>
            </div>
            </div>
            </div>
            <?php endwhile; ?> 
        </div>
        <?php endif; ?>
        <?php if( have_rows('button_group') ): ?>
        <div class="button_group text-center">
            <?php while ( have_rows('button_group') ) : the_row(); ?>
            <a class="btn" href="<?php the_sub_field('url'); ?>" <?php if(get_sub_field('new_window')): ?> target="_blank"<?php endif; ?>><?php the_sub_field('title'); ?></a>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
</section>