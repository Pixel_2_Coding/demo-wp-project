<section id="<?php the_sub_field('section_id'); ?>" class="bloc_section bs_two_column register-sec content_sec_<?php echo $GLOBALS['i']; ?> <?php the_sub_field('section_class'); ?>" style="<?php if(get_sub_field('background_color')):?>background-color:<?php the_sub_field('background_color'); ?>;<?php endif; ?><?php if(get_sub_field('text_color')):?>color:<?php the_sub_field('text_color'); ?>;<?php endif; ?>">
    <div class="container <?php the_sub_field('container_size');?>">
        <?php if(get_sub_field('heading') || get_sub_field('sub_heading')): ?>
        <div class="text-center sec_head" data-aos="zoom-in-up">
            <?php if(get_sub_field('heading')): ?>
            <h2 class="heading"><?php the_sub_field('heading'); ?></h2>
            <?php endif; ?>
            <?php if(get_sub_field('sub_heading')): ?>
            <h3 class="sub_heading"><?php the_sub_field('sub_heading'); ?></h3>
            <?php endif; ?>
            <?php if(get_sub_field('show_icons')): ?>
            <div class="shape_01 text-center"><img src="<?php bloginfo('template_directory'); ?>/images/shape_01.svg" alt=""></div>
            <?php endif; ?>
        </div>
        <?php endif; ?>
        
        <?php if(get_sub_field('left_column') || get_sub_field('right_column')): ?>
        <div class="row column-content">
            
            <?php if(get_sub_field('left_column')): ?>
            <div class="<?php the_sub_field('left_column_class'); ?>">
                <div data-aos="zoom-in-right">
                <?php while ( have_rows('left_column') ) : the_row(); ?>
                <div class="col-right-space">
                    
                    <h2><?php the_sub_field('heading'); ?></h2>
                    <div><?php the_sub_field('content'); ?></div>
                    
                </div>
                <div class="form_area">
                    
                    <h5><?php the_sub_field('form_heading'); ?></h5>
                    <div><?php the_sub_field('form_code'); ?></div>
                    
                </div>
                </div>
                <?php endwhile; ?>
                
                </div><?php endif; ?>
            
            
                <div class="<?php the_sub_field('right_column_class'); ?>">
                    <div data-aos="zoom-in-up">
                    <?php if( have_rows('right_column') ): ?>
                    <?php while ( have_rows('right_column') ) : the_row(); ?>
                    <div class="find-us_box"><img src="<?php the_sub_field('image_overlay_icon'); ?>" alt="" class="find-us-top-image" /><img src="<?php the_sub_field('image'); ?>" alt="" /><a class="btn btn-black mapshow" href="Javascript:void(0);"><?php the_sub_field('map_button_label'); ?></a></div>
                    <?php endwhile; ?>
                <?php endif; ?>    
                </div>
                </div>
                
        </div>
        
        <?php if(get_sub_field('show_icons')): ?>
        <div class="shape_02 text-center" data-aos="zoom-in-up"><img src="<?php bloginfo('template_directory'); ?>/images/shape_02.svg" alt=""></div>
        <?php endif; ?>
        <?php endif; ?>
        <?php if( have_rows('button_group') ): ?>
        <div class="button_group text-center" data-aos="zoom-in-up">
            <?php while ( have_rows('button_group') ) : the_row(); ?>
            <a class="btn btn-default" href="<?php the_sub_field('url'); ?>" <?php if(get_sub_field('new_window')): ?> target="_blank"<?php endif; ?>><?php the_sub_field('title'); ?></a>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
    
    <div class="map_popup_register transitionAll">
        <a class="mapcloseicon" href="javascript:void(0);"><img src="<?php bloginfo('template_directory'); ?>/images/close_icon.svg" alt=""></a>
        <?php the_field('map','option'); ?>
    </div>
</section>