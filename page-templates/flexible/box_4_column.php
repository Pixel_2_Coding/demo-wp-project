<section id="<?php the_sub_field('section_id'); ?>" class="bloc_section bs_four_column content_sec_<?php echo $GLOBALS['i']; ?> <?php the_sub_field('section_class'); ?>" style="<?php if(get_sub_field('background_color')):?>background-color:<?php the_sub_field('background_color'); ?>;<?php endif; ?><?php if(get_sub_field('text_color')):?>color:<?php the_sub_field('text_color'); ?>;<?php endif; ?>">
    <div class="container <?php the_sub_field('container_size');?>">
        <?php if(get_sub_field('heading') || get_sub_field('sub_heading')): ?>
        <div class="text-center sec_head" data-aos="zoom-in-up">
            <?php if(get_sub_field('heading')): ?>
            <h2 class="heading"><?php the_sub_field('heading'); ?></h2>
            <?php endif; ?>
            <?php if(get_sub_field('sub_heading')): ?>
            <h3 class="sub_heading"><?php the_sub_field('sub_heading'); ?></h3>
            <?php endif; ?>
        </div>
        <?php endif; ?>
        <?php if( have_rows('box') ): ?>
        <div class="row">
        <?php $b=0; while ( have_rows('box') ) : the_row(); $b++; ?>
            <div class="col-lg-3 col-md-6 col-sm-6 col-6 col-xxs-12 <?php the_sub_field('text_align'); ?>">
                <div class="box" data-aos="zoom-in-up" data-aos-delay="<?php echo $b;?>00">

                    <?php
                    $image = get_sub_field('image');

                    // vars
                    $url = $image['url'];
                    //$title = $image['title'];
                    //$alt = $image['alt'];
                    //$caption = $image['caption'];

                    // thumbnail
                    $size = 'box-thumb';
                    $thumb = $image['sizes'][ $size ];
                    //$width = $image['sizes'][ $size . '-width' ];
                    //$height = $image['sizes'][ $size . '-height' ];

                    ?>
                    <div class="box_thumb_title">
                    <?php if( $thumb ): ?>
                    <img src="<?php echo $thumb; ?>" alt="">
                    <?php endif;  ?>

                    <?php if(get_sub_field('title')): ?>
                        <div class="box_title text-center"><div class="box_title_inner"><h3><?php the_sub_field('title');  ?></h3></div></div>
                    <?php endif; ?>
                    </div>
                    <div class="box_copy">
                    <?php if(get_sub_field('sub_title')): ?>
                    <h4><?php the_sub_field('sub_title');  ?></h4>
                    <?php endif; ?>
                    <?php if(get_sub_field('description')): ?>
                    <div class="description"><?php the_sub_field('description');  ?></div>
                    <?php endif; ?>
                    <?php if(get_sub_field('button_label')): ?>
                    <div class="box_btn"><a href="<?php the_sub_field('button_url');  ?>" <?php if(get_sub_field('open_new_tab')): ?> target="_blank" <?php endif; ?> ><?php the_sub_field('button_label');  ?></a></div>
                    <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
</section>
