<?php if(get_sub_field('show_menu_section')): ?> 
<section id="<?php the_sub_field('section_id'); ?>" class="bloc_section bs_menu content_sec_<?php echo $GLOBALS['i']; ?> <?php the_sub_field('section_class'); ?>" style="<?php if(get_sub_field('background_color')):?>background-color:<?php the_sub_field('background_color'); ?>;<?php endif; ?><?php if(get_sub_field('text_color')):?>color:<?php the_sub_field('text_color'); ?>;<?php endif; ?>">
    <div class="container <?php the_sub_field('container_size');?>">
        <?php if(get_sub_field('heading','option') || get_sub_field('sub_heading','option')): ?>
        <div class="text-center sec_head" data-aos="zoom-in-up">
            <?php if(get_sub_field('heading','option')): ?>
            <h2 class="heading"><?php the_sub_field('heading','option'); ?></h2>
            <?php endif; ?>
            <?php if(get_sub_field('sub_heading','option')): ?>
            <h3 class="sub_heading"><?php the_sub_field('sub_heading','option'); ?></h3>
            <?php endif; ?>
        </div>
        <?php endif; ?>











        <?php if( have_rows('select_menu') ): ?>
        <ul class="nav nav-tabs" id="myTab" role="tablist" data-aos="zoom-in-up">
        <?php $m=0; while ( have_rows('select_menu') ) : the_row(); $m++; ?>

            <li class="nav-item <?php if($m === 1): ?> active<?php endif; ?>"><a class="btn btn-default" id="menu-name<?php echo $m; ?>-tab" data-toggle="tab" data-target="#menu-name<?php echo $m; ?>" role="tab" aria-controls="menu-name<?php echo $m; ?>" aria-selected="true"><?php the_sub_field('menu_category'); ?></a></li>


        <?php endwhile; ?>
        </ul>
        <?php endif; ?>


        <div class="shape_01 text-center" data-aos="zoom-in-up"><img src="<?php bloginfo('template_directory'); ?>/images/shape_01.svg" alt=""></div>



         <?php if( have_rows('select_menu') ): ?>
        <div class="tab-content" id="myTabContent" data-aos="zoom-in-up">
        <?php $n=0; while ( have_rows('select_menu') ) : the_row(); $n++; ?>

            <div class="tab-pane fade show <?php if($n === 1): ?> active<?php endif; ?> " id="menu-name<?php echo $n; ?>" role="tabpanel" aria-labelledby="menu-name<?php echo $n; ?>-tab">

                <?php while ( have_rows('menu_list') ) : the_row(); ?>

                <?php

                $ids = get_sub_field('menu_item', false, false);



                $post_args = array(
	'post_type'      	=> 'menu',
	'posts_per_page'	=> -1,
	'post__in'			=> $ids,
	'post_status'		=> 'any',
	'orderby'        	=> 'post__in',
                ); ?>
                <?php $wp_query = new WP_Query($post_args); ?>
                <?php if ($wp_query->have_posts()) : ?>
                <div class="accordion_item" id="accordion_<?php echo $category->term_id; ?>">
                    <?php $o = 0; while ($wp_query->have_posts()) : $wp_query->the_post(); $o++; ?>
                    <div class="menu-card">
                        <div class="menu-card-header" id="heading<?php echo get_the_ID(); ?>">
                            <a href="JavaScript:void(0);" class="acclink collapsed<?php /* if($o > 1): ?>collapsed<?php endif; */ ?>" data-toggle="collapse" data-target="#collapse<?php echo get_the_ID(); ?>" aria-expanded="true" aria-controls="collapse<?php echo get_the_ID(); ?>"><?php the_title(); ?><span class="acc_arrow"><img src="<?php bloginfo('template_directory'); ?>/images/arrow_01.svg" alt=""></span></a>
                        </div>
                        <div id="collapse<?php echo get_the_ID(); ?>" class="collapse <?php /* if($o === 1): ?> show<?php endif; */ ?> " aria-labelledby="heading<?php echo get_the_ID(); ?>" data-parent="#accordion_<?php echo $category->term_id; ?>">
                            <div class="menu-card-body">
                                <div class="menu_item_list text-center">
                                <?php if( have_rows('menu_list') ): ?>
                                <?php while ( have_rows('menu_list') ) : the_row(); ?>
                                    <div class="menu_list_box">
                                    <h4 class="menu_title"><?php the_sub_field('title'); ?></h4>
                                    <div class="menu_description"><?php the_sub_field('description'); ?></div>
                                    </div>
                                <?php endwhile; ?>

                                <?php else: ?>
                                    <div class="menu_list_box">
                                    <h4 class="menu_title"><?php _e('Menu Item Not listed'); ?></h4>
                                    </div>
                                <?php endif; ?>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>

                </div>
                <?php endif; ?>
                <?php wp_reset_query(); ?>

            <?php endwhile; ?>
            </div>


        <?php endwhile; ?>
        </div>
        <?php endif; ?>

        <div class="shape_02 text-center" data-aos="zoom-in-up"><img src="<?php bloginfo('template_directory'); ?>/images/shape_02.svg" alt=""></div>
        <?php if( have_rows('button_group') ): ?>
        <div class="button_group text-center" data-aos="zoom-in-up">
            <?php while ( have_rows('button_group') ) : the_row(); ?>
            <a class="btn btn-default" href="<?php the_sub_field('url'); ?>" <?php if(get_sub_field('new_window')): ?> target="_blank"<?php endif; ?>><?php the_sub_field('title'); ?></a>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
</section>
<?php endif; ?>
