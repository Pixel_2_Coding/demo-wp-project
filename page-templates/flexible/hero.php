<section id="<?php the_sub_field('section_id'); ?>" class="heroSec content_sec content_sec_<?php echo $GLOBALS['i']; ?> <?php the_sub_field('section_class'); ?>">
    <div class="site-slider">
        <?php while ( have_rows('hero_image') ) : the_row(); ?>
        <div class="hero_itm" style="background-image:url('<?php the_sub_field('image'); ?>'); "></div>
        <?php endwhile; ?>
    </div>
    
    <?php if(get_sub_field('show_scroll_arrow')): ?>
    <div class="bingJourney" data-aos="zoom-in" data-aos-delay="700" data-aos-anchor=".header-container">
        <a href="<?php the_sub_field('target_id'); ?>"><img src="<?php the_sub_field('scroll_icon'); ?>" alt=""><?php the_sub_field('scroll_text'); ?></a>
    </div>
    <?php endif; ?>
</section>