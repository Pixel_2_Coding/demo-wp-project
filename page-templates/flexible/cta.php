<section id="<?php the_sub_field('section_id'); ?>" class="section_cta text-center content_sec_<?php echo $GLOBALS['i']; ?> <?php the_sub_field('section_class'); ?>" style="<?php if(get_sub_field('background_color')):?>background-color:<?php the_sub_field('background_color'); ?>;<?php endif; ?><?php if(get_sub_field('text_color')):?>color:<?php the_sub_field('text_color'); ?>;<?php endif; ?>">
    <div class="image-bg" style="<?php if(get_sub_field('background_image')):?>background-image:url(<?php the_sub_field('background_image'); ?>);<?php endif; ?>"><div class="bg_shape"></div></div>
    <div class="container <?php the_sub_field('container_size');?>">
        <?php if(get_sub_field('icon')): ?>
        <div class="cta_icon"><a href="<?php the_sub_field('icon_url'); ?>"><img src="<?php the_sub_field('icon'); ?>" alt=""></a></div>
        <?php endif; ?>
        <?php if(get_sub_field('heading')): ?>
        <h3><a href="<?php the_sub_field('heading_url'); ?>"><?php the_sub_field('heading'); ?></a></h3>
        <?php endif; ?>
    </div>
</section>
