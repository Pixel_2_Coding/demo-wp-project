<section id="<?php the_sub_field('section_id'); ?>" class="bloc_section bs_hsc content_sec_<?php echo $GLOBALS['i']; ?> <?php the_sub_field('section_class'); ?>" style="<?php if(get_sub_field('background_color')):?>background-color:<?php the_sub_field('background_color'); ?>;<?php endif; ?><?php if(get_sub_field('text_color')):?>color:<?php the_sub_field('text_color'); ?>;<?php endif; ?>">
    <div class="container <?php the_sub_field('container_size');?>">
        <?php if(get_sub_field('heading') || get_sub_field('sub_heading') || get_sub_field('icon')): ?>
        <div class="text-center sec_head" data-aos="zoom-in-up">
            <?php if(get_sub_field('icon')): ?>
            <div class="head-icon"><a <?php if(get_sub_field('icon_url')): ?>href="<?php the_sub_field('icon_url'); ?>" <?php endif; ?> ><img src="<?php the_sub_field('icon'); ?>" alt=""></a></div>
            <?php endif; ?>
            <?php if(get_sub_field('heading')): ?>
            <h2 class="heading"><?php the_sub_field('heading'); ?></h2>
            <?php endif; ?>
            <?php if(get_sub_field('sub_heading')): ?>
            <h3 class="sub_heading"><?php the_sub_field('sub_heading'); ?></h3>
            <?php endif; ?>
        </div>
        <?php endif; ?>
    </div>
<?php if( have_rows('slider') ): ?>
    <div class="site-slider">
        <?php while ( have_rows('slider') ) : the_row(); ?>
        <div class="slide_item"><img src="<?php the_sub_field('image'); ?>" alt=""></div>
        <?php endwhile; ?>
    </div>
    <?php endif; ?>
        <div class="container <?php the_sub_field('container_size');?>">
     <?php if(get_sub_field('left_column') || get_sub_field('right_column')): ?>
        <div class="row column-content">

            <?php if(get_sub_field('left_column') || get_sub_field('left_column_heading')): ?>
            <div class="col-sm-6">
                <div data-aos="zoom-in-right">
                <?php if(get_sub_field('left_column_heading')): ?><h2><?php the_sub_field('left_column_heading'); ?></h2><?php endif; ?>
                <?php the_sub_field('left_column'); ?>
            </div>
            </div>
            <?php endif; ?>

            <?php if(get_sub_field('right_column') || get_sub_field('right_column_heading')): ?>
            <div class="col-sm-6">
                <div data-aos="zoom-in-up">
                <?php if(get_sub_field('right_column_heading')): ?><h2><?php the_sub_field('left_column_heading'); ?></h2><?php endif; ?>
                <?php the_sub_field('right_column'); ?>
                </div>
            </div>
            <?php endif; ?>

        </div>
        <?php endif; ?>

    <?php if( have_rows('button_group') ): ?>
        <div class="button_group text-center" data-aos="zoom-in-up">
            <?php while ( have_rows('button_group') ) : the_row(); ?>
            <a class="btn btn-default" href="<?php the_sub_field('url'); ?>" <?php if(get_sub_field('new_window')): ?> target="_blank"<?php endif; ?>><?php the_sub_field('title'); ?></a>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
</section>
