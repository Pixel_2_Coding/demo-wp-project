<section id="<?php the_sub_field('section_id'); ?>" class="sliderSec content_sec content_sec_<?php echo $GLOBALS['i']; ?> <?php the_sub_field('section_class'); ?>">
    <div class="site-slider">
        <?php while ( have_rows('slider') ) : the_row(); ?>
        <div class="slide_item"><img src="<?php the_sub_field('image'); ?>" alt=""></div>
        <?php endwhile; ?>
    </div>
</section>