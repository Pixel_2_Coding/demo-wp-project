equalheight = function(container){

    var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        jQueryel,
        topPosition = 0;
    jQuery(container).each(function() {

        jQueryel = jQuery(this);
        jQuery(jQueryel).height('auto')
        topPostion = jQueryel.position().top;

        if (currentRowStart != topPostion) {
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0; // empty the array
            currentRowStart = topPostion;
            currentTallest = jQueryel.height();
            rowDivs.push(jQueryel);
        } else {
            rowDivs.push(jQueryel);
            currentTallest = (currentTallest < jQueryel.height()) ? (jQueryel.height()) : (currentTallest);
        }
        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
}

function sidebarMenuHeight(){
    var windowsHeight = jQuery(window).height();
    var navcloserHeight = jQuery('.sidebarmenuicon').outerHeight();
    var sidebarFooterHeight = jQuery('.sidebar_footer').outerHeight();
    var sidebarnavbarHeight = jQuery('.sidebarMenu .nav.navbar-nav').outerHeight();
    /*jQuery('.sidebarMenu .menu-sidebar-menu-container').css({"padding-top": navcloserHeight, "font-size": "200%"});*/
    jQuery('.sidebarMenu .menu-sidebar-menu-container').css({"padding-top": navcloserHeight, "padding-bottom": windowsHeight - navcloserHeight - sidebarFooterHeight - sidebarnavbarHeight});
    /*jQuery('.sidebarMenu .nav.navbar-nav').css({});*/
}


function menuHeight(){
    var windowsHeight = jQuery(window).height();
    var navHeight = jQuery('.site_header').outerHeight();
    /*jQuery('.mainMenuContainer').css('max-height',windowsHeight-navHeight);*/
    jQuery('.mainMenuScroller').css('max-height',windowsHeight-navHeight);
}

function halfCircle(){
    var boxTitleWidthe = jQuery('.box_title').outerWidth();
    jQuery('.box_title').css('height',boxTitleWidthe);
}

function cta_bg_shape(){
    var shapewidth = jQuery('.image-bg').outerHeight();
    jQuery('.bg_shape').css('width',shapewidth);
}

function offsetLeft(){
    var offset = jQuery('.container').offset();
    if(offset && offset != 'undefined')
    {
        var left = offset.left;
    }
    jQuery('.box_left_inner').css('padding-left',left+15);
}




function error404(){
    var windowsHeight = jQuery(window).height();
    var footerHeight = jQuery('.footer-section').outerHeight();
    jQuery('.error404 #content').css('min-height',windowsHeight - footerHeight );
    jQuery('.error404 #content').addClass('row');
}



jQuery(window).scroll(function() {
    var scroll = jQuery(window).scrollTop();

    if (scroll >= 38) {
        jQuery("body").addClass("fixed");
        jQuery("body").removeClass("notFixed");
    } else {
        jQuery("body").removeClass("fixed");
        jQuery("body").addClass("notFixed");
    }
});



jQuery(document).ready(function(){

    jQuery('.home a[href*=#]').each(function () {
        /*$(this).attr('href', $(this).attr('href').replace('/#', '#'));*/
        jQuery(this).attr('href', jQuery(this).attr('href').replace('/#', '#'));
    });

    /*// scrollTo if #_ found
    hashname = window.location.hash.replace('#_', '');
    // find element to scroll to (<a name=""> or anything with particular id)
    elem = $('a[name="' + hashname + '"],#' + hashname);

    if(elem) {
         $(document).scrollTo(elem, 800);
    }

    */







    jQuery(".news_search a").click(function(){
        jQuery('.search_bar').slideToggle('slow');
        jQuery('#search').focus();

    });

    jQuery(".mapshow").click(function(){
        jQuery('.map_popup_register').addClass('show_map');
    });

    jQuery(".map_popup_register .mapcloseicon").click(function(){
        jQuery('.map_popup_register').removeClass('show_map');
    });



    jQuery(".mapshowlink").click(function(){
        jQuery('.map_popup').addClass('show_map');
    });
    jQuery(".map_popup .mapcloseicon").click(function(){
        jQuery('.map_popup').removeClass('show_map');
    });

    jQuery(".mobileMenuArrow").click(function(){
        jQuery(this).next(".sub-menu").slideToggle("slow");
    });

    jQuery(".icon_hamburger").click(function(){
        jQuery(".mainMenu").toggleClass("openmainMenu");
        jQuery(".mainMenuScroller").slideToggle("slow");
        jQuery(this).toggleClass("menu_active");
        jQuery(".mainMenuScroller").toggleClass("show-menu");
        jQuery(".sidebarMenu").toggleClass("openmenu");
    });
    jQuery(".sidebarmenuicon .icon_hamburger_close").click(function(){
      jQuery( ".icon_hamburger" ).trigger( "click" );
        //jQuery(".sidebarMenu").removeClass('openmenu');
    });

    jQuery(".mainMenu ul li a").click(function(){
      jQuery( ".icon_hamburger" ).trigger( "click" );
    });

    jQuery(".navbar-nav a").click(function(){
      jQuery( ".icon_hamburger" ).trigger( "click" );
    });
    jQuery( ".navbar-nav" ).find( ".sub-menu" ).addClass("dropdown-menu");

    jQuery('.search-results').parents('html').removeClass('transparent_header');
    jQuery('body.error404').parents('html').addClass('transparent_header');

    jQuery('.hero_slider').slick({
        dots: false,
        nextArrow: '<div class="slArrow slick-arrow-right"><i class="fa fa-angle-right"></i></div>',
        prevArrow: '<div class="slArrow slick-arrow-left"><i class="fa fa-angle-left"></i></div>'
    });

    jQuery('.site-slider').slick({
        dots: false,
        autoplay: true,
        autoplaySpeed: 4000,
        adaptiveHeight: true
    });


   // offsetLeft();
    equalheight('.equalheight');
    menuHeight();
    halfCircle();
    cta_bg_shape();
    sidebarMenuHeight();
    error404();

});

jQuery(window).load(function(){
    /*offsetLeft();*/
    equalheight('.equalheight');
    menuHeight();
    halfCircle();
    cta_bg_shape();
    sidebarMenuHeight();
    error404();


    jQuery('.jQueryEqualHeight').jQueryEqualHeight('.cardbox .post_content h2');
    jQuery('.jQueryEqualHeight').jQueryEqualHeight('.cardbox .post_content .post_excerpt');
    jQuery('.jQueryEqualHeight').jQueryEqualHeight('.cardbox');


});

jQuery(window).resize(function(){
    /*offsetLeft();*/
    equalheight('.equalheight');
    menuHeight();
    halfCircle();
    cta_bg_shape();
    sidebarMenuHeight();
    error404();

        jQuery('.jQueryEqualHeight').jQueryEqualHeight('.cardbox .post_content h2');
    jQuery('.jQueryEqualHeight').jQueryEqualHeight('.cardbox .post_content .post_excerpt');
    jQuery('.jQueryEqualHeight').jQueryEqualHeight('.cardbox');
});




function onScroll(event){
    var scrollPos = jQuery(document).scrollTop();
    jQuery('.mainMenuScroller .menu li a[href^="#"]').each(function () {
        var currLink = jQuery(this);
        var refElement = jQuery(currLink.attr("href").replace('/',''));
        if (refElement.length) {
        if (refElement.position().top-80 <= scrollPos && refElement.position().top-80 + refElement.outerHeight() > scrollPos) {
            jQuery('.mainMenuScroller .menu li').removeClass("active");
            jQuery(this).parent('li').addClass("active");
        }
        else{
            currLink.removeClass("active");
        }
       }
    });

    jQuery('.sidebar_scroller .nav li a[href^="#"]').each(function () {
        var currLink = jQuery(this);
        var refElement = jQuery(currLink.attr("href").replace('/',''));
        if (refElement.length) {
        if (refElement.position().top-80 <= scrollPos && refElement.position().top-80 + refElement.outerHeight() > scrollPos) {
            jQuery('.sidebar_scroller .nav li').removeClass("active");
            jQuery(this).parent('li').addClass("active");
        }
        else{
            currLink.removeClass("active");
        }
       }
    });
}

jQuery(document).ready(function () {
    jQuery(document).on("scroll", onScroll);

    /*smoothscroll*/
    jQuery('a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        //jQuery('.sidebarMenu').removeClass('openmenu');
        jQuery(document).off("scroll");

        jQuery('a').each(function () {
            jQuery(this).parent('li').removeClass('active');
        })
        jQuery(this).parent('li').addClass('active');

        var target = this.hash,
            menu = target;
        jQuerytarget = jQuery(target);
        jQuery('html, body').stop().animate({
            'scrollTop': jQuerytarget.offset().top-60
        }, 500, 'swing', function () {
            /*window.location.hash = target;*/
            jQuery(document).on("scroll", onScroll);
        });
    });
});