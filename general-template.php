<?php
/**
 Template Name: General Page
 */
get_header(); ?>
<div class="news_bar">
    <div class="container clearfix">
        <div class="news_back"><a onclick="goBack()" href="Javascript:void(0);" ><span>&lt;</span> BACK</a></div>
        <div class="news_heading">&nbsp;</div>
        <?php /*<div class="news_search"><a href="Javascript:void(0);"><img src="<?php bloginfo('template_directory'); ?>/images/icon_search.svg" alt=""></a></div> */ ?>
    </div>
    <?php /*
    <div class="search_bar">
    <div class="container">
        <form action="/" method="get">
            <div class="clearfix search_bg"><input type="text" name="s" id="search" placeholder="Search..." value="<?php the_search_query(); ?>" /><button type="submit"><i class="fas fa-search"></i></button>
            </div>
        </form>
    </div>
</div> */ ?>

</div>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>> 
    <div class="container">
    <div class="container-news">
        <?php if ( has_post_thumbnail() ): ?>
        <div class="post_image"><?php the_post_thumbnail('full'); ?></div>
            <?php endif; ?> 
        
        <div class="entry-content">
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                    <?php the_content(); ?>
                </div>
        <div class="">
        <ul class="share-icons">
            <?php /*<li><span>SHARE THIS</span></li>
            <li><a target="_blank" href="https://www.facebook.com/sharer?u=<?php the_permalink();?>&t=<?php the_title(); ?>"><i class="fab fa-facebook"></i></a></li>
            <li><a target="_blank" href="http://twitter.com/intent/tweet?text=Currently reading <?php the_title(); ?>&amp;url=<?php the_permalink(); ?>" title="Click to share this post on Twitter"><i class="fab fa-twitter"></i></a></li>*/ ?>
        </ul> 
        </div>
    </div>
</div>
</article><!-- #post-## -->
<?php endwhile; ?>

<?php get_footer(); ?>