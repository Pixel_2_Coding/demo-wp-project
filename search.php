<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */

get_header(); ?>

<div class="news_bar">
    <div class="container clearfix">
        <div class="news_back"><a onclick="goBack()" href="Javascript:void(0);" > &lt; BACK</a></div>
        <div class="news_heading">&nbsp;</div>
        <div class="news_search"><a href="Javascript:void(0);"><img src="<?php bloginfo('template_directory'); ?>/images/icon_search.svg" alt=""></a></div> 
    </div>
    <div class="search_bar">
    <div class="container">
        <form action="/" method="get">
            <div class="clearfix search_bg"><input type="text" name="s" id="search" placeholder="Search..." value="<?php the_search_query(); ?>" /><button type="submit"><i class="fas fa-search"></i></button>
            </div>
        </form>
    </div>
</div>

</div>

<article class="large_post col-md-12">
    <div class="container">
    <div class="row">
        <div class="<?php if ( has_post_thumbnail() ): ?>col-md-7 order-2 order-md-1<?php else: ?>col-md-12<?php endif; ?>">
            <div class="post_content">
            <h2 class="search_result_heading"><?php printf( __( 'Search Results for: %s', 'boilerplate' ), '' . get_search_query() . '' ); ?></h2>
            </div>
        </div>
    </div>
    <div class="divider"></div>
    </div>
</article>
<?php if ( have_posts() ) : ?><div class="container">
    <div class="row">
<?php $p=0; while ( have_posts() ) : the_post(); $p++; ?>
<?php if($p <= 2): ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class('large_post col-md-12'); ?>>
    <div class="row">
        <div class="<?php if ( has_post_thumbnail() ): ?>col-md-7 order-2 order-md-1<?php else: ?>col-md-12<?php endif; ?>">
            <div class="post_content">
            <h2><?php the_title(); ?></h2>
            <div class="post_excerpt"><?php the_excerpt(); ?></div>
            <div class="post_url"><a href="<?php the_permalink(); ?>">...READ MORE</a></div>
            </div>
        </div>
        <?php if ( has_post_thumbnail() ): ?>
        <div class="col-md-5 post-image order-1 order-md-2">
            <?php the_post_thumbnail('post-slider'); ?>
        </div>
        <?php endif; ?> 
    </div>
        <div class="divider"></div>
        
</article><!-- #post-## -->
    <?php else: ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class('small_post col-md-4 jQueryEqualHeight'); ?>>
    
        <div class="post-box cardbox">
            <?php if ( has_post_thumbnail() ): ?>
            <?php the_post_thumbnail('post-small'); ?>
            <?php endif; ?> 
            <div class="post_content">
            <h2 cl><?php the_title(); ?></h2>
            <div class="post_excerpt"><?php echo get_excerpt(195); ?></div>
            <div class="post_url"><a href="<?php the_permalink(); ?>">...READ MORE</a></div>
            </div>
        </div>
        
    
</article><!-- #post-## -->
    <?php endif; ?>
<?php endwhile; ?>
    </div></div>
<div class="container text-center"><?php wp_pagenavi(); ?></div>
<?php else : ?> 

<article id="post-not-found"> 
    <div class="container">
        <div class="row post-row">
            <div class="postBroder"></div>
            
            
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="post-content">
                    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php _e( 'Nothing Found'); ?></a></h2>
                    <div class="post-excerpt"><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.' ); ?></div>
                    
                </div>
            </div>
        </div>
    </div>
</article><!-- #post-## -->

<?php endif; ?>



<?php get_footer(); ?>
