<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */

get_header(); ?>
<div class="container"><h1 class="page-title"><?php printf( __( 'Category Archives: %s', 'boilerplate' ), '' . single_cat_title( '', false ) . '' ); ?></h1></div>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>> 
    <div class="container">
        <div class="row post-row">
            <div class="postBroder"></div>
            <?php if( get_field('hero_image') ): ?>
            <div class="col-xxs-12 col-xs-4 col-sm-4 col-md-3 post_thumbnail">
                <a href="<?the_permalink(); ?>" style="background-image:url(<?php the_field('hero_image'); ?>)"><img src="<?php bloginfo('template_directory'); ?>/images/gallery-thumb.png"></a>
                
                </div>
                <?php endif; ?>
            
            <div class="<?php if( get_field('hero_image') ): ?>col-xxs-12 col-xs-8 col-sm-8 col-md-9<?php else: ?>col-xs-12 col-sm-12 col-md-12<?php endif; ?>">
                <div class="post-content">
                    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <div class="postInfo"><span class="posrted"><strong>Posted:</strong> <?php the_time('jS F Y') ?></span> <span><strong>Author:</strong> <?php the_author(); ?></span></div>
                    <div class="post-excerpt"><?php the_excerpt(); ?></div>
                    <div class="post-btn-sec"><a href="<?php the_permalink(); ?>" class="btn btn-post">Read More</a></div>
                    
                </div>
            </div>
        </div>
    </div>
</article><!-- #post-## -->
<?php endwhile; ?>

<div class="container text-center"><?php wp_pagenavi(); ?></div>

<?php get_footer(); ?>